package storages

import (
	"gitlab.com/konfka/boilerplate/internal/db/adapter"
	"gitlab.com/konfka/boilerplate/internal/infrastructure/cache"
	vstorage "gitlab.com/konfka/boilerplate/internal/modules/auth/storage"
	ustorage "gitlab.com/konfka/boilerplate/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
