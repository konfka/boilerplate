package modules

import (
	"gitlab.com/konfka/boilerplate/internal/infrastructure/component"
	aservice "gitlab.com/konfka/boilerplate/internal/modules/auth/service"
	uservice "gitlab.com/konfka/boilerplate/internal/modules/user/service"
	"gitlab.com/konfka/boilerplate/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
